import './App.css';
import React, { useEffect, useState } from 'react';
import { GiShoppingCart } from 'react-icons/gi';
import List from './components/List';
import Alert from './components/Alert';

const getLocalStorage = () => {
  let list = localStorage.getItem('list');
  if (list) {
    return (list = JSON.parse(localStorage.getItem('list')));
  } else {
    return [];
  }
};

const App = () => {
  const [name, setName] = useState('');
  const [list, setList] = useState(getLocalStorage());
  const [isEditing, setIsEditing] = useState(false);
  const [editID, setEditID] = useState(null);
  const [alert, setAlert] = useState({ show: false, msg: '', type: '' });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!name) {
      showAlert(true, 'bg-danger', 'please enter value');
    } else if (name && isEditing) {
      setList(
        list.map((item) => {
          if (item.id === editID) {
            return { ...item, title: name };
          }
          return item;
        })
      );
      setName('');
      setEditID(null);
      setIsEditing(false);
      showAlert(true, 'bg-success', 'value changed');
    } else {
      showAlert(true, 'bg-success', 'item added to the list');
      const newItem = { id: new Date().getTime().toString(), title: name };

      setList([...list, newItem]);
      setName('');
    }
  };

  const showAlert = (show = false, type = '', msg = '') => {
    setAlert({ show, type, msg });
  };
  const clearList = () => {
    showAlert(true, 'bg-danger', 'empty list');
    setList([]);
  };
  const removeItem = (id) => {
    showAlert(true, 'bg-danger', 'item removed');
    setList(list.filter((item) => item.id !== id));
  };
  const editItem = (id) => {
    const specificItem = list.find((item) => item.id === id);
    setIsEditing(true);
    setEditID(id);
    setName(specificItem.title);
  };
  useEffect(() => {
    localStorage.setItem('list', JSON.stringify(list));
  }, [list]);

  return (
    <div className="min-h-screen text-black overflow-hidden">
      {/* <div className="container mx-auto mt-10"> */}
      <div className="max-w-2xl mx-4 sm:max-w-sm md:max-w-sm lg:max-w-sm xl:max-w-sm sm:mx-auto md:mx-auto lg:mx-auto xl:mx-auto mt-16 rounded-lg">
        <div className="flex flex-col justify-center items-center px-2 py-4 shadow-2xl bg-white bg-opacity-75">
          <header className="flex flex-col justify-center sm:flex-row sm:justify-start space-x-3 px-2 py-2 sm:py-3">
            <h1 className="inline-flex align-middle text-3xl text-center xs:text-left">
              <GiShoppingCart className=" align-middle" />
              <span className="inline-flex align-middle">Grocery Bud</span>
            </h1>
          </header>

          <form className="space-x-1" onSubmit={handleSubmit}>
            {alert.show && (
              <Alert {...alert} removeAlert={showAlert} list={list} />
            )}

            <input
              type="text"
              className="px-2 py-2 rounded focus:outline-none focus:ring focus:border-pink-200"
              placeholder="E.g. Carton Milk"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <button
              type="submit"
              className="px-2 py-2 bg-blue-200 hover:bg-blue-300"
            >
              {isEditing ? 'Edit' : 'Add'}
            </button>
          </form>

          {/* <div className="flex flex-row px-1 py-1"> */}
          {list.length > 0 && (
            <div className="py-8">
              <List items={list} removeItem={removeItem} editItem={editItem} />
              <button
                type="submit"
                className="w-full mt-9 px-2 py-1 rounded-md bg-red-300 hover:text-white hover:bg-bg-danger"
                onClick={clearList}
              >
                Clear
              </button>
            </div>
          )}
          {/* </div> */}
        </div>
      </div>
    </div>
  );
}

export default App;
