import React, { useEffect } from 'react'

const Alert = ({ type, msg, removeAlert, list }) => {
  useEffect(() => {
    const timeout = setTimeout(() => {
      removeAlert();
    }, 10000);
    return () => clearTimeout(timeout);
  }, [list, removeAlert]);

  return (
    <p className={`px-2 py-1 capitalize text-center mb-1 text-white ${type}`}>
      {msg}
      <span className="bg-danger hidden"></span>
      <span className="bg-success hidden"></span>
    </p>
  );
};

export default Alert
