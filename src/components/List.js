import React from 'react'
import { FaEdit, FaTrash } from 'react-icons/fa';

const List = ({ items, removeItem, editItem }) => {
  return (
    <div className="flex flex-col">
      {items.map((item) => {
        const { id, title } = item
        return (
          <div
            className="flex justify-between mb-2"
            key={id}
          >
            <span className="text-xl">{title}</span>
            <span className="inline-flex align-middle px-5">
              <button type="button" className="" onClick={() => editItem(id)}>
                <FaEdit className="mr-2 text-blue-400 hover:text-blue-500 text-2xl" />
              </button>
              <button type="button" className=" w-full" onClick={() => removeItem(id)}>
                <FaTrash className="text-red-400 hover:text-red-500 text-2xl" />
              </button>
            </span>
          </div>
        );
        
      })}
    </div>
  );
};

export default List
